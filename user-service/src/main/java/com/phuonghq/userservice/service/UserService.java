package com.phuonghq.userservice.service;

import com.phuonghq.userservice.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(int id);
    User getUserByName(String userName);
    User getUserByNameOrEmail(String userName, String email);
    User saveUser(User user);
}
