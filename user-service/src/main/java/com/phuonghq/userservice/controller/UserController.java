package com.phuonghq.userservice.controller;

import com.phuonghq.userservice.model.User;
import com.phuonghq.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping (value = "/users")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping (value = "/users", params = "name")
    public User getUserByName(@RequestParam("name") String userName){
        return userService.getUserByName(userName);
    }

    @GetMapping (value = "/users/{id}")
    public User getUserById(@PathVariable("id") int id){
        return userService.getUserById(id);
    }

    @PostMapping (value = "/users")
    public ResponseEntity<User> addUser(@RequestBody User userDTO, HttpServletRequest request) throws URISyntaxException {
        User user = userService.getUserByNameOrEmail(userDTO.getUserName(), userDTO.getEmail());
        if (user != null) return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
        userDTO.setCreatdAt(new Date());
        userDTO.setRole("USER");
        userService.saveUser(userDTO);
        return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
    }
}
