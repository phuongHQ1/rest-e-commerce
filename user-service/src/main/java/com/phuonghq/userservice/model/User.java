package com.phuonghq.userservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table (name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (name = "email", unique = true)
	private String email;

    @Column (name = "user_name", unique = true)
    private String userName;

    @Column (name = "password")
    private String password;

    @Column (name = "active" )
    private int active;

	@Column (name = "created_at" )
	@NotNull
	private Date creatdAt;

	@Column (name = "updated_at" )
	private Date updatedAt;

	@Column (name = "access_token")
	private String accessToken;

    @Column (name = "role" )
    private String role;
}
