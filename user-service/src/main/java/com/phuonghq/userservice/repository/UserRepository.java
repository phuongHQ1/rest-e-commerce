package com.phuonghq.userservice.repository;

import com.phuonghq.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUserName(String userName);
    User findByUserNameOrEmail(String userName, String email);
}
