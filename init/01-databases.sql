# create databases
CREATE DATABASE IF NOT EXISTS `product`;
CREATE DATABASE IF NOT EXISTS `order`;

# create root user and grant rights
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';