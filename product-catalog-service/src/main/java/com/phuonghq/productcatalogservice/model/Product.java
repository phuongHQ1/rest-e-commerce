package com.phuonghq.productcatalogservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table (name = "products")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "name")
    @NotNull
    private String name;

    @Column (name = "price")
    @NotNull
    private BigDecimal price;

    @Column (name = "description")
    private String description;

	@Column (name = "brand")
    @NotNull
	private String brand;

	@Column (name = "color")
    @NotNull
	private String color;

    @Column (name = "category")
    private String category;

    @Column (name = "availability")
    @NotNull
    private int availability;

	@Column (name = "is_active")
	@NotNull
	private boolean isActive;

	@Column (name = "created_at")
	@NotNull
	private Date createdAt;

	@Column (name = "updated_at")
	private Date updatedAt;
}
