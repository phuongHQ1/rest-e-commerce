package com.phuonghq.productcatalogservice.controller;

import com.phuonghq.productcatalogservice.service.ProductService;
import com.phuonghq.productcatalogservice.model.Product;
import com.phuonghq.common.response.BaseResponseData;
import com.phuonghq.common.response.Meta;
import com.phuonghq.common.util.CPagging;
import com.phuonghq.common.util.OffsetBasedPageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.phuonghq.common.controller.BaseController;


import java.util.List;

@RestController

public class ProductController extends BaseController {

    @Autowired
    private ProductService productService;

    @GetMapping (value = "/products")
    public List<Product> getAllProducts(){
        return productService.getAllProduct();
    }

    @GetMapping(value = "/products", params = "category")
    public List<Product> getAllProductByCategory(@RequestParam ("category") String category){
        return productService.getAllProductByCategory(category);
    }

    @GetMapping (value = "/products/{id}")
    public ResponseEntity<?> getOneProductById(@PathVariable ("id") long id){
        BaseResponseData respData = new BaseResponseData();
        respData.setData(productService.getOneById(id));
        return new ResponseEntity<>(respData, HttpStatus.OK);
    }

    @GetMapping (value = "/products", params = "name")
    public List<Product> getAllProductsByName(@RequestParam ("name") String name){
        return productService.getAllProductsByName(name);
    }

    @GetMapping("/products/list")
    @ResponseBody
    public ResponseEntity<?> search( @RequestParam(name = CPagging.OFFEST_NAME, defaultValue = CPagging.STR_DEFAULT_OFFEST) Integer offset,
                                          @RequestParam(name = CPagging.LIMIT_NAME, defaultValue = CPagging.STR_DEFAULT_LIMIT) Integer limit,
                                          @RequestParam(name= "name",defaultValue = "-ALL-")String name,
                                          @RequestParam(name = "price",defaultValue = "-1") String price,
                                          @RequestParam(name = "color",defaultValue = "-ALL-") String color,
                                          @RequestParam(name = "brand",defaultValue = "-ALL-") String brand,
                                          @RequestParam(name = "sort_by",defaultValue = "id") String sortBy,
                                          @RequestParam(name = "order_by",defaultValue = "asc") String orderBy
    ) throws Exception{

        Sort.Order orderOperator = orderBy.equals("ASC") ? Sort.Order.asc(sortBy) : Sort.Order.desc(sortBy);
        Sort orders = Sort.by(orderOperator);
        OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(offset, limit, orders);

        return success(productService.search(name, price, color, brand, pageRequest), offset, limit);
    }
    @Override
    public Meta getErrorMeta(Exception ex) {
        return null;
    }
}
