package com.phuonghq.productcatalogservice.controller;

import com.phuonghq.productcatalogservice.service.AuditService;
import com.phuonghq.productcatalogservice.service.ProductService;
import com.phuonghq.productcatalogservice.model.Product;
import com.phuonghq.common.response.Meta;
import com.phuonghq.common.util.CPagging;
import com.phuonghq.common.util.OffsetBasedPageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import com.phuonghq.common.controller.BaseController;


@RestController
@RequestMapping("/admin")
public class AdminProductController extends BaseController{

    @Autowired
    private ProductService productService;

    @Autowired
    private AuditService auditService;

    @PostMapping(value = "/products")
    private ResponseEntity<Product> addProduct(@RequestBody Product product, HttpServletRequest request) throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(request.getRequestURI() + "/" + product.getId()));
        productService.addProduct(product);
        return new ResponseEntity<>(product, headers, HttpStatus.CREATED);
    }

    @PutMapping(value = "/products/{id}")
    private ResponseEntity<Product> updateProduct(@RequestBody Product product,@PathVariable Long id,  HttpServletRequest request) throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(request.getRequestURI() + "/" + product.getId()));
        productService.update(product, id);
        return new ResponseEntity<>(product, headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/products/{id}")
    private ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id){
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/products/audit")
    private ResponseEntity<Product> getProductAudit(@RequestParam(name="subject") String subject, @RequestParam(name="action") String action,
                                                    @RequestParam(name = CPagging.OFFEST_NAME, defaultValue = CPagging.STR_DEFAULT_OFFEST) Integer offset,
                                                    @RequestParam(name = CPagging.LIMIT_NAME, defaultValue = CPagging.STR_DEFAULT_LIMIT) Integer limit)  {
        Sort orders = Sort.by(Sort.Order.desc("createdAt"));
        OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(offset, limit, orders);

        return success(auditService.getAudits(action, subject, pageRequest), offset, limit);
    }

    @GetMapping(value = "/tracking/products/{id}")
    private ResponseEntity<Void> trackingProductPrice(@PathVariable("id") Long id){
        Product product = productService.getOneById(id);
        if (product == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        productService.trackingProductPrice("PRODUCT", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @Override
    public Meta getErrorMeta(Exception ex) {
        return null;
    }
}
