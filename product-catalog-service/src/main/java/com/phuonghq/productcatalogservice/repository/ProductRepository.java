package com.phuonghq.productcatalogservice.repository;

import com.phuonghq.productcatalogservice.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByCategory(String category);
    List<Product> findAllByName(String name);

    @Query("FROM Product p  WHERE (UPPER(p.name) = ?1 OR ?1 = '-ALL-') AND (p.price = ?2 OR ?2 = '-1') AND (UPPER(p.color) = ?3 OR ?3 = '-ALL-' ) AND (UPPER(p.brand) = ?4 OR ?4 = '-ALL-') ")
    Page<Product> seach(String name, BigDecimal price, String color, String brand, Pageable pageable);

}
