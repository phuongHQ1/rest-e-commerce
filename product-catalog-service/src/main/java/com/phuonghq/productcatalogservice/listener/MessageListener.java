package com.phuonghq.productcatalogservice.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phuonghq.productcatalogservice.service.AuditService;
import com.phuonghq.common.beans.EventData;
import com.phuonghq.common.entity.Audit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * Created by phuong.huynh
 */
@Component
public class MessageListener {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuditService auditService;

    @KafkaListener(topics = {"TOPIC.PRODUCT.SEARCH","TOPIC.PRODUCT.VIEW_DETAIL"}, groupId = "test-grp")
    public void productEventListener(String message) {
        try {
            EventData event = objectMapper.readValue(message, EventData.class);
            Audit audit= new Audit();
            audit.setSubject("PRODUCT");
            audit.setPayload(event.getPayload().toString());
            audit.setCreatedAt(new Date());
            audit.setAction(event.getEvent());
            auditService.addAudit(audit);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
