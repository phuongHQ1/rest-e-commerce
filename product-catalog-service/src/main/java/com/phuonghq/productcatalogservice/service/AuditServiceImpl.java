package com.phuonghq.productcatalogservice.service;

import com.phuonghq.common.entity.Audit;
import com.phuonghq.common.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AuditServiceImpl implements AuditService{

    @Autowired
    private AuditRepository auditRepository;


    @Override
    public Audit addAudit(Audit audit) {
        return auditRepository.save(audit);
    }

    @Override
    public Page<Audit> getAudits (String action, String subject, Pageable pageable){
        return auditRepository.findByActionAndSubject(action, subject, pageable);
    }
}
