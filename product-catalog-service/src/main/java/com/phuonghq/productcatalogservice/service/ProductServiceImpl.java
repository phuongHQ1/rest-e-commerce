package com.phuonghq.productcatalogservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.phuonghq.productcatalogservice.model.Product;
import com.phuonghq.productcatalogservice.repository.ProductRepository;
import com.phuonghq.common.beans.EventData;
import com.phuonghq.common.beans.ProductBean;
import com.phuonghq.common.entity.Version;
import com.phuonghq.common.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private VersionRepository versionRepository;

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }


    @Override
    public List<Product> getAllProductByCategory(String category) {
        return productRepository.findAllByCategory(category);
    }

    @Override
    public Product getOneById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        broadcastProductEvent(ProductBean.valueOf(id, null, null, null, null), "VIEW_DETAIL","TOPIC.PRODUCT.VIEW_DETAIL");
        return product.orElse(null);
    }

    @Override
    public List<Product> getAllProductsByName(String name) {
        return productRepository.findAllByName(name);
    }

    @Override
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public ResponseEntity update(Product productDTO, Long id ) {
        Optional<Product> opProduct = productRepository.findById(id);
        if (!opProduct.isPresent() ) return ResponseEntity.notFound().build();
        productDTO.setId(id);
        productDTO.setCreatedAt(opProduct.get().getCreatedAt());
        productDTO.setUpdatedAt(new Date());

        if (!opProduct.get().getPrice().equals(productDTO.getPrice())){
            versionRepository.save(Version.valueOf("PRODUCT", "CHANGE_PRICE", id, null,productDTO.toString() ));
        }
        productRepository.save(productDTO);
        return ResponseEntity.noContent().build();
    }

    public Page<Product> search(String name, String price, String color, String brand, Pageable pageable){
        Page<Product> products =  productRepository.seach(name.toUpperCase(), new BigDecimal(price), color.toUpperCase(), brand.toUpperCase(), pageable);
        broadcastProductEvent(ProductBean.valueOf(null, name, price, brand, color), "SEARCH","TOPIC.PRODUCT.SEARCH");
        return products;
    }


    @Override
    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }

    private void broadcastProductEvent(ProductBean productBean, String event, String topic) {
        EventData eventData = EventData.valueOf(null, event, null
                , productBean);
        try {
            kafkaTemplate.send(topic, objectMapper.writeValueAsString(eventData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Version> trackingProductPrice(String itemType, Long productId) {
        return versionRepository.getByItemTypeAndSubjectId(itemType, productId);
    }
}
