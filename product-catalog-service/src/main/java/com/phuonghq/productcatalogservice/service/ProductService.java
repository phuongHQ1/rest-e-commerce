package com.phuonghq.productcatalogservice.service;

import com.phuonghq.common.entity.Version;
import com.phuonghq.productcatalogservice.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
     List<Product> getAllProduct();
     List<Product> getAllProductByCategory(String category);
     Product getOneById(Long id);
     List<Product> getAllProductsByName(String name);
     Page<Product> search(String name, String price, String color, String brand, Pageable pageRequest);
     Product addProduct(Product product);
     ResponseEntity update(Product product, Long id );
     void deleteProduct(Long productId);
     List<Version> trackingProductPrice(String itemType, Long productId);
}
