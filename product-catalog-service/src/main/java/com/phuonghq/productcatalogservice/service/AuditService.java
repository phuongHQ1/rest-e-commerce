package com.phuonghq.productcatalogservice.service;


import com.phuonghq.common.entity.Audit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuditService {
    Audit addAudit (Audit audit);
    Page<Audit> getAudits (String action, String subject, Pageable pageable);
}
