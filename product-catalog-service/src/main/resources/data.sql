INSERT INTO `products` (`id`, `description`, `name`, `price`, `brand`, `color`, `is_active`, `availability`, `created_at`, `category` ) VALUES
  (1,'LEGO Cup test','Coffee Cup',4.99,'LEGO','white',true,5,now(), null),
  (2,'We are providing Mike Headphones. Just test.','Mike Headphone',24.99,'Mike','gray',true,10,now(),null),
  (4,'Monitor Stand ','Monitor Stand',99.99,'HP','black',true,4,now(),null),
  (5, 'this is cool hat', 'Hat', 19.99, 'HAT hat', 'pink', true, 9, now(),null),
  (6, 'KeyBoard DELL', 'Keyboard', 4.99, 'DELL', 'black',true, 8, now(),null);

