package com.phuonghq.productcatalogservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phuonghq.productcatalogservice.model.Product;
import com.phuonghq.productcatalogservice.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class controllerTests {

    private static final String PRODUCT_NAME= "test";
    private static final Long PRODUCT_ID = 100L;
    private static final String PRODUCT_CATEGORY = "testCategory";

    private List<Product> products;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Before
    public void setUp(){
        Product product = new Product();
        product.setName(PRODUCT_NAME);
        product.setId(PRODUCT_ID);
        product.setCategory(PRODUCT_CATEGORY);
        products = new ArrayList<>();
        products.add(product);

    }

    @Test
    public void  get_all_products_controller_test() throws Exception {
        given(productService.getAllProduct()).willReturn(products);

        mockMvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id").value(PRODUCT_ID))
                .andExpect(jsonPath("$[0].name").value(PRODUCT_NAME))
                .andDo(print());

        verify(productService, Mockito.times(1)).getAllProduct();
        verifyNoMoreInteractions(productService);
    }

    @Test
    public void get_all_product_by_category_controller_test() throws Exception {

        given(productService.getAllProductByCategory(PRODUCT_CATEGORY)).willReturn(products);

        mockMvc.perform(get("/products").param("category", PRODUCT_CATEGORY))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id").value(PRODUCT_ID))
                .andExpect(jsonPath("$[0].category").value(PRODUCT_CATEGORY))
                .andDo(print());


        verify(productService, times(1)).getAllProductByCategory(anyString());
        verifyNoMoreInteractions(productService);
    }


//    @Test
//    public void add_product_controller_test() throws Exception {
//        String productJson = objectMapper.writeValueAsString(product);
//        when(productService.addProduct(new Product())).thenReturn(product);
//
//        mockMvc.perform(post("/products").content(productJson).contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.id").value(PRODUCT_ID))
//                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
//                .andExpect(jsonPath("$.category").value(PRODUCT_CATEGORY))
//                .andDo(print());
//
//        verify(productService, times(1)).addProduct(product);
//        verifyNoMoreInteractions(productService);
//    }TODO

//    @Test
//    public void get_one_product_by_id_controller_test() throws Exception {
//
//        when(productService.getOneById(anyLong())).thenReturn(product);
//
//        mockMvc.perform(get("/products/{id}", PRODUCT_ID))
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
////                .andExpect(jsonPath("$.id").value(PRODUCT_ID))TODO
//                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
//                .andExpect(jsonPath("$.category").value(PRODUCT_CATEGORY))
//                .andDo(print());
//
//        verify(productService, times(1)).getOneById(PRODUCT_ID);
//        verifyNoMoreInteractions(productService);
//    }
}
