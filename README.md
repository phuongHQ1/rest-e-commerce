# REST Microservices architecture


### Table of contents

- [Project architecture](#Project-architecture)
- [Tools and Technologies](#technologies)
- [Features](#features)
- [Principle-Pattern](#Principle-pattern)
- [Code structure explaination](#code_structures)
- [How to run](#how_to_run)
- [Testing](#testing)
- [APIs](#apis)


### Project architecture
 ![58799788-845b1c00-8606-11e9-924b-1b4c03a9091c__2_](/uploads/20861af1b79799f7cb3ad9f22ce93cf6/58799788-845b1c00-8606-11e9-924b-1b4c03a9091c__2_.png)


### ERD Diagrram

+ User
![user_schema](/uploads/5ecf0c5ae6cb7e2edf0aaef2769d1a30/user_schema.png)
+ Product
![product_schema](/uploads/6f9e88c8561181caea11579eed5be9f7/product_schema.png)
 + Order
 ![order_schema](/uploads/aae9632b285842cf4e43d3988552a6ca/order_schema.png)
 

### Tools and Technologies

- **Java 8**
- **Spring Boot** 
- **Apache Kafka**
- **Spring Cloud - Open Feign**
- **Netflix Eureka Client/Server**
- **Redis Client : JEDIS**
- **Spring Data JPA** 
- **Hibernate**
- **SQL Database engine** : MYSQL
- **NOSQL Database engine** : Redis
- **Maven**

### Features

1. **Administrator :**

   - Users management
   - Products management
   - Orders management
   - Tracking price
   - Audit

2. **User** :

   - Registration
   - Shopping cart
   - Order
   - Product catalog

### Principle-pattern
In this project, I followed by:
- Software principles: SOLID, DRY, KISS...
- Design patterns: Factory, Builder, Observe, Adapter, Singleton, Iterator...


### Code structures
We got totally 6 sub-folder (for 5 services + 1 common lib):
+ eureka-server: Working as service registry. All internal service will register with this service. This service will know all each other
+ api-gateway: All service will be exposed for external by gateway. This is also Eureka server client
+ common: A lib for common entity, repository and util function..Will be added in each service as jar file
+ user-service: Responsible for all user: add user, get all user, get user by id ...
+ product-catalog-service: Service take care for all product management, tracking price and audit for admin...
+ order-service: allow add product to cart and proceed order.


### How to run
All service is dockerized into only one docker-compose.yml file.
We only need to easily run `docker-compose build && docker-compose up` and wait for some minutes to all service start.
After that, go to http://localhost:8761/ and look at in `Instances currently registered with Eureka
` section to see if it start successfully. In expectation, we'll see 4 services 

### Testing
This project cover unit test and integration test using JUnit, Mockito and SpringBootTest. Because we're using @SpringBootTest,
(and we're not using any embedded database like H2, Derby...)
it's really matter that we have to run kafka, mysql.. before run test

### APIs
Because there are lots of endpoint, so I won't go through all of them. I'll show curl for some significant endpoint<br>

**For USER**
- Get all user:<br>
`curl --location --request GET 'http://localhost:8900/api/accounts/users' \
--header 'Content-Type: application/json'` <br>
By default, I created 2 user `admin` and `user`right after service start up. You can use this or create another by API:<br>
`curl --location --request POST 'http://localhost:8900/api/accounts/users' \
 --header 'Content-Type: application/json' \
 --data-raw '{"email": "customer1@test.com",
         "user_name": "customer1",
         "password": "123456"}'`
         <br>
 Please remember email AND user are unique. Or it will return 422 HTTP Code <br>
Otherwise, there are some endpoint as `getUserById`, `getUserByName`...

**For Product**
- Get all products: <br>
`curl --location --request GET 'http://localhost:8900/api/catalog/products' \
 --header 'Content-Type: application/json' `<br>
- Get product by Id: <br>
`curl --location --request GET 'http://localhost:8900/api/catalog/products/2' \
 --header 'Content-Type: application/json' \`
 
- Get products which satisfied filter, sort, searching condition<br>
`curl --location --request GET 'http://localhost:8900/api/catalog/products/list?color=black&price=4.99&name=name&brand=brand&sort_by=name&order_by=asc&offset=1&limit=5' \
  --header 'Content-Type: application/json' \` <br>
 This endpoint also support filter different criteria such as name, price, brand, colour and pagination. By default, if offset is 0 and limit is 20<br>
 
 Some other endpoints: `getById`, `getByName`
- For admin, we got some feature like: add a product, delete product...
+ Update product:<br>
`curl --location --request PUT 'http://localhost:8900/api/catalog/admin/products/2' \
 --header 'Content-Type: application/json' \
 --data-raw ' {
         "price": 10.99,
         "description": "LEGO Cup test",
         "brand": "LEGO",
         "color": "white1",
         "category": "test",
         "availability": 5,
         "active": true,
         "name":"tmp"
  }'`
 If the price change, we will record that change into ourDB <br>
 `curl --location --request GET 'http://localhost:8900/api/catalog/admin/tracking/products/2' \
  --header 'Content-Type: application/json' \` with 2 is product's ID <br>
 + For audit support, all customers' activities such as searching, filtering and
   viewing product's details need to be stored in the database.<br>
   -> Kafka will handle that. I use product-service as producer and consumer as well<br>
   `curl --location --request GET 'http://localhost:8900/api/catalog/admin/products/audit?subject=PRODUCT&action=VIEW_DETAIL' \
    --header 'Content-Type: application/json' \
`
<br>

**For Order**

Order service provide features: `AddItemToCart`, `RemoveItemToCart` and `create order`, also show payment method and calculate all of price 
   + Add item to cart<br>
   `curl --location --request POST 'http://localhost:8900/api/shop/cart?product_id=4&quantity=2' \
    --header 'Content-Type: application/json' \
    --data-raw ' {
           "cart_id":"phuonghq1"
     }'` <br>
     In this endpoint, order service actually call product service to check info about product using Feign Client and store cart info to redis
    <br>
    
   + Remove item out of cart <br>
    `curl --location --request DELETE 'http://localhost:8900/api/shop/cart?product_id=1' \
     --header 'Content-Type: application/json' \
     --data-raw '{"cart_id":"phuonghq1"}'` <br>
   + Creat order<br>
    `curl --location --request POST 'http://localhost:8900/api/shop/order?user_id=2' \
     --header 'Content-Type: application/json' \
     --data-raw '{
     "cart_id":"phuonghq1"}'`<br>
     Example response look like that:
     ```{
            "id": 2,
            "orderedDate": "2020-09-15",
            "status": "PROCESSING",
            "total": 214.95,
            "paymentMethod": "COD",
            "items": [
                {
                    "quantity": 3,
                    "subTotal": 14.97,
                    "product": {
                        "id": 1,
                        "name": "Coffee Cup",
                        "price": 4.99,
                        "brand": "LEGO",
                        "color": "white"
                    }
                },
                {
                    "quantity": 2,
                    "subTotal": 199.98,
                    "product": {
                        "id": 4,
                        "name": "Monitor Stand",
                        "price": 99.99,
                        "brand": "HP",
                        "color": "black"
                    }
                }
            ],
            "user": {
                "id": 2,
                "userName": "customer"
            }
        }
        
    Order service call user service to check user info, before update into it's db
    