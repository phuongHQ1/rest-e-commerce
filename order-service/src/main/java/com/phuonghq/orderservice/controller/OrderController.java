package com.phuonghq.orderservice.controller;

import com.phuonghq.orderservice.model.User;
import com.phuonghq.orderservice.feignclient.UserClient;
import com.phuonghq.orderservice.model.Item;
import com.phuonghq.orderservice.model.Order;
import com.phuonghq.orderservice.service.CartService;
import com.phuonghq.orderservice.service.OrderService;
import com.phuonghq.orderservice.utilities.OrderUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private UserClient userClient;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CartService cartService;

    @PostMapping(value = "/order")
    private ResponseEntity<?> saveOrder(@RequestParam("user_id") Long userId, @RequestBody String cartId){
        Order order = new Order();
        List<Item> cart = cartService.getAllItemsFromCart(cartId);
        User user = userClient.getOneUser(userId);
        order.setItems(cart);
        order.setUser(user);
        order.setTotal(OrderUtilities.countTotalPrice(cart));
        order.setOrderedDate(LocalDate.now());
        order.setStatus("PROCESSING");
        order.setPaymentMethod("COD");

        try{
            orderService.saveOrder(order);
            cartService.deleteCart(cartId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }
}
