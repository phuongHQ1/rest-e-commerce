package com.phuonghq.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table (name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long productId;

    @Transient
    private Long id;

    @Column (name = "name")
    @NotNull
    private String name;

    @Column (name = "price")
    @NotNull
    private BigDecimal price;

    @Column (name = "brand")
    private String brand;

    @Column (name = "color")
    private String color;

    @OneToMany (mappedBy = "product", cascade = CascadeType.MERGE)
    @JsonIgnore
    private List<Item> items;
}
