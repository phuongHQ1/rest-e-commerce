package com.phuonghq.orderservice.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "product-catalog-service", url = "${feign.client.product-service.url}")
public interface ProductClient {

    @GetMapping(value = "/products/{id}")
    ResponseEntity<?> getProductById(@PathVariable(value = "id") Long productId);

}
