package com.phuonghq.orderservice.feignclient;

import com.phuonghq.orderservice.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "User", url = "${feign.client.user-service.url}")
public interface UserClient {

    @GetMapping(value = "/users/{id}")
    User getOneUser(@PathVariable("id") Long id);
}
