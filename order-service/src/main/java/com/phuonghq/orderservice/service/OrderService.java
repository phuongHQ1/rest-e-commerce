package com.phuonghq.orderservice.service;

import com.phuonghq.orderservice.model.Order;

public interface OrderService {
    Order saveOrder(Order order);
}
