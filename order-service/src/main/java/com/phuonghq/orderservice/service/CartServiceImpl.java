package com.phuonghq.orderservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phuonghq.orderservice.feignclient.ProductClient;
import com.phuonghq.orderservice.model.Product;
import com.phuonghq.orderservice.redis.CartRedisRepository;
import com.phuonghq.orderservice.utilities.CartUtilities;
import com.phuonghq.orderservice.model.Item;
import com.phuonghq.common.response.BaseResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductClient productClient;

    @Autowired
    private CartRedisRepository cartRedisRepository;

    @Override
    public void addItemToCart(String cartId, Long productId, Integer quantity) throws Exception {
        ResponseEntity<?> resp = productClient.getProductById(productId);
        if (resp.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Call product service failed");
        }
        BaseResponseData respData = objectMapper.convertValue(resp.getBody(), BaseResponseData.class);
        assert respData != null;
        Product product = objectMapper.convertValue(respData.getData(), Product.class);
        if (product!= null) {
            Item item = new Item(quantity, product, CartUtilities.getSubTotalForItem(product, quantity));
             cartRedisRepository.addItemToCart(cartId, item);
        }
        else throw new Exception("Product is NOT FOUND");
    }

    @Override
    public List<Object> getCart(String cartId) {
        return (List<Object>)cartRedisRepository.getCart(cartId, Item.class);
    }

    @Override
    public void changeItemQuantity(String cartId, Long productId, Integer quantity) {
        List<Item> cart = (List) cartRedisRepository.getCart(cartId, Item.class);
        for(Item item : cart){
            if((item.getProduct().getId()).equals(productId)){
                cartRedisRepository.deleteItemFromCart(cartId, item);
                item.setQuantity(quantity);
                item.setSubTotal(CartUtilities.getSubTotalForItem(item.getProduct(),quantity));
                cartRedisRepository.addItemToCart(cartId, item);
            }
        }
    }

    @Override
    public void deleteItemFromCart(String cartId, Long productId) {
        List<Item> cart = (List) cartRedisRepository.getCart(cartId, Item.class);
        for(Item item : cart){
            if((item.getProduct().getId()).equals(productId)){
                cartRedisRepository.deleteItemFromCart(cartId, item);
            }
        }
    }

    @Override
    public boolean checkIfItemIsExist(String cartId, Long productId) {
        List<Item> cart = (List) cartRedisRepository.getCart(cartId, Item.class);
        for(Item item : cart){
            if((item.getProduct().getId()).equals(productId)){
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Item> getAllItemsFromCart(String cartId) {
        List<Item> items = (List)cartRedisRepository.getCart(cartId, Item.class);
        return items;
    }

    @Override
    public void deleteCart(String cartId) {
        cartRedisRepository.deleteCart(cartId);
    }
}
