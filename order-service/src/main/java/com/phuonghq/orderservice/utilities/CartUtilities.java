package com.phuonghq.orderservice.utilities;

import com.phuonghq.orderservice.model.Product;
import java.math.BigDecimal;

public class CartUtilities {

    public static BigDecimal getSubTotalForItem(Product product, int quantity){
       return (product.getPrice()).multiply(BigDecimal.valueOf(quantity));
    }
}
