package com.phuonghq.common.controller;

import com.phuonghq.common.response.BaseResponseData;
import com.phuonghq.common.response.Meta;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Author phuong.huynh
 */
public abstract class BaseController {

    protected abstract Meta getErrorMeta(Exception ex);



    protected ResponseEntity<?> getUnknowErrorResponse(BaseResponseData respData, Exception e) {
        respData.setMeta(getErrorMeta(e));
        respData.setData(null);
        return new ResponseEntity<Object>(respData, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    protected <T> ResponseEntity success(T data) {
        BaseResponseData wrap = new BaseResponseData();
        wrap.setData(data);
        return new ResponseEntity<Object>(wrap, HttpStatus.OK);
    }

    protected <T> ResponseEntity success(T data, int offset, int limit, int total) {
        BaseResponseData wrap = new BaseResponseData();
        wrap.setData(data);
//        wrap.setMeta(responseUtil.getMetaList(CErrorCode.SUCCESS, offset, limit, total));
        return new ResponseEntity<Object>(wrap, HttpStatus.OK);
    }

    protected ResponseEntity success(Page page, int offset, int limit) {
        BaseResponseData r = new BaseResponseData();
//        r.setMeta(responseUtil.getMetaList(CErrorCode.SUCCESS, offset, limit, (int)page.getTotalElements()));
        r.setData(page.getContent());
        return new ResponseEntity(r, HttpStatus.OK);
    }
    protected <T> ResponseEntity successWithoutWrapper(T data) {
        return new ResponseEntity<Object>(data, HttpStatus.OK);
    }
}
