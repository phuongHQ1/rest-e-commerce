package com.phuonghq.common.repository;

import com.phuonghq.common.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VersionRepository extends JpaRepository<Version, Long> {
    List<Version> getByItemTypeAndSubjectId(String itemType, Long id);
}
