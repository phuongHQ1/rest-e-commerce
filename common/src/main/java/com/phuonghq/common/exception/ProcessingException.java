package com.phuonghq.common.exception;

/**
 * Author phuong.huynh
 */
public class ProcessingException extends RuntimeException {

	private static final long serialVersionUID = 1179122009625188236L;
	public int code;

    public ProcessingException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
