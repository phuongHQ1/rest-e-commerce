package com.phuonghq.common.util;

/**
 * @author phuong.huynh8
 *
 */
public class CPagging {
	public static final int INT_DEFAULT_OFFEST = 0;
	public static final int INT_DEFAULT_LIMIT = 20;
	
	public static final String OFFEST_NAME = "offset";
	public static final String LIMIT_NAME = "limit";
	
	public static final String STR_DEFAULT_OFFEST = "0";
	public static final String STR_DEFAULT_LIMIT = "20";
}
