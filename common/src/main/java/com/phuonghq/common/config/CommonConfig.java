package com.phuonghq.common.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by phuong.huynh
 */
@Configuration
public class CommonConfig {

    @Bean
    public Mapper mapper() {
        Mapper mapper = new DozerBeanMapper();
        return mapper;
    }
}
