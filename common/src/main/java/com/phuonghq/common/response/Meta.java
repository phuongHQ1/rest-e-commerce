package com.phuonghq.common.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Author phuong.huynh
 */
@Data
public class Meta implements Serializable {
    private static final long serialVersionUID = -645749963423423317L;

    @JsonProperty("code")
    private int code = HttpStatus.OK.value();

    @JsonProperty("message")
    private String message = "Success";
}
