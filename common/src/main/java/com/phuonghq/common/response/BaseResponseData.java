package com.phuonghq.common.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @author phuong.huynh8
 *
 */

@Data
public class BaseResponseData implements Serializable {

	private static final long serialVersionUID = -6457499697558273317L;

	@JsonProperty("meta")
	private Meta meta = new Meta();

	@JsonProperty("data")
	private Object data;
}
