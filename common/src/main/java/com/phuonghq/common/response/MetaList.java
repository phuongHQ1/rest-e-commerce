//package com.phuonghq.common.response;
//
///**
// * Author phuong.huynh
// */
//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//@ToString
//public class MetaList extends Meta {
//    int offset;
//    int limit;
//    int total;
//
//    @Builder(builderMethodName = "childBuilder")
//    public MetaList(int code, String message, int offset, int limit, int total) {
//        super(code, message);
//        this.offset = offset;
//        this.limit = limit;
//        this.total = total;
//    }
//}
