package com.phuonghq.common.beans;

import lombok.Data;

/**
 * Created by phuong.huynh
 */
@Data
public class ProductBean {
    private Long productId;
    private String name;
    private String price;
    private String brand;
    private String color;

    public static ProductBean valueOf(Long id, String name, String price, String brand, String color){
        ProductBean productBean = new ProductBean();
        productBean.setProductId(id);
        productBean.setName(name);
        productBean.setPrice(price);
        productBean.setBrand(brand);
        productBean.setColor(color);
        return productBean;
    }
}
