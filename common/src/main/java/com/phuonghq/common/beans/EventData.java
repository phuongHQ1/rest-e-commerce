package com.phuonghq.common.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by phuong.huynh
 */
@Getter
@Setter
public class EventData {
    private String id;
    private String event;


    @JsonProperty("timestamp")
    private long timeStamp;

    @JsonProperty("user_id")
    private String userId;
    private Object payload;

    @JsonProperty("payload_id")
    private String payloadId;

    public static EventData valueOf(String userId, String event, String payloadId, Object payload){
        EventData eventData = new EventData();
        eventData.setId(UUID.randomUUID().toString());
        eventData.setEvent(event);
        eventData.setUserId(userId);
        eventData.setTimeStamp(System.currentTimeMillis());
        eventData.setPayloadId(payloadId);
        eventData.setPayload(payload);
        return eventData;
    }

}
