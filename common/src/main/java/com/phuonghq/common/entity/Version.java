package com.phuonghq.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "versions")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Version {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "item_type")
    private String itemType;

    @Column (name = "event")
    private String event;

    @Column (name = "user_id")
    private Long userId;

    @Column (name = "subject_id")
    private Long subjectId;

    @Column (name = "object")
    private String object;

	@Column(name = "created_at")
	private Date createdAt;

    public static Version valueOf(String itemType, String event, Long subjectId, Long userId, String object){
        Version version = new Version();
        version.setItemType(itemType);
        version.setEvent(event);
        version.setSubjectId(subjectId);
        version.setUserId(userId);
        version.setObject(object);
        version.setCreatedAt(new Date());

        return version;
    }
}
