package com.phuonghq.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "audits")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Audit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "subject")
    private String subject;

    @Column (name = "action")
    private String action;

    @Column (name = "payload")
    private String payload;

	@Column(name = "created_at")
	private Date createdAt;
}
